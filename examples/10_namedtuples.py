from collections import namedtuple

#####################################
# Namedtuples enable ad-hoc records #
#####################################

upw_line = namedtuple('MetraLine', 'line from_ to')('UP-W', 'Ogilvie Transportation Center', 'Elburn')
print(upw_line.line)    # Prints "UP-W"

#########################################################
# Namedtuple factories can help you create more records #
#########################################################

MetraLineFactory = namedtuple('MetraLine', 'line from_ to')
some_other_source = ['MD-W', 'Union Station', 'Elgin']

mdw_line = MetraLineFactory(*some_other_source)
print(mdw_line)  # Prints MetraLine(line='MD-W', from_='Union Station', to='Elgin')

# They're still tuples, though

mdw_line.to = 'Albuquerque'     # AttributeError: can't set attribute
