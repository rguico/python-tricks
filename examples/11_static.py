class CampaignService:
    replacements = {
        '{chant}': 'hi ho',
        '{secret_power}': 'hadoken',
    }

    def __init__(self):
        self.id = 1
        self.message = ''

    def update_message(self, msg: str):
        self.message = msg
        for old, new in CampaignService.replacements.items():
            self.message = self.message.replace(old, new)

    @classmethod
    def get_replacements(cls):
        return cls.replacements

    @staticmethod
    def send():
        print('Sending stuff but can\'t access replacements or self!')


c = CampaignService()
c.update_message('Team Campaigns: {chant}! {secret_power}')
print(c.message)    # Outputs: Team Campaigns: hi ho! hadoken

print(CampaignService.get_replacements())   # Outputs: {'{chant}': 'hi ho', '{secret_power}': 'hadoken'}

CampaignService.send()  # Outputs: Sending stuff but can't access replacements or self!
