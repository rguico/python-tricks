def regular_generator():
    yield 1
    yield 2
    yield 3
    yield 'foo'
    yield 123


x = regular_generator()
print(next(x))  # 1
print(next(x))  # 2
print(next(x))  # 3
print(next(x))  # foo
print(next(x))  # 123

try:
    print(next(x))
except StopIteration:
    print('A stop iteration was raised.')

for x in regular_generator():
    print(x)    # Prints the same as lines 10-14


class Robert:
    def __init__(self):
        self.idx = 0
        self.robert = 'robert'

    def __iter__(self):
        return self

    def __next__(self):
        if self.idx == len(self.robert):
            raise StopIteration
        ret = self.robert[self.idx]
        self.idx += 1
        return ret


r = Robert()
for x in r:
    print(x)    # Prints robert with each letter on its own line
