from collections import defaultdict
from datetime import datetime
from time import sleep
import json

# Files

with open('temp/myfile', 'w') as f:
    f.write('O hai!')

# Not files

db = defaultdict(lambda: {
    'status': 0,
    'cdate': datetime.isoformat(datetime.now()),
    'udate': datetime.isoformat(datetime.now())
})


class Campaign:
    def __init__(self, id):
        self.id = id

    def __enter__(self):
        return db[self.id]

    def __exit__(self, exc_type, exc_val, exc_tb):
        sleep(2)
        db[self.id]['udate'] = datetime.isoformat(datetime.now())
        return False


with Campaign(2) as campaign:
    campaign['status'] = '7'

print(json.dumps(db, indent=4))
