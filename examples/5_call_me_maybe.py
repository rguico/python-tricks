class Formatter:
    def __init__(self, tag):
        self.tag = tag

    def __call__(self, my_string):
        return f"<{self.tag}>{my_string}</{self.tag}>"


marqueer = Formatter('marquee')
blinker = Formatter('blink')

print(marqueer('foo'))
print(blinker('bar'))
