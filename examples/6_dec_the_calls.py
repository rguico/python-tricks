def add_blink_tag(the_original_function):
    def wrapper(parameter1, parameter2):
        return f"<blink>{the_original_function(parameter1, parameter2)}</blink>"

    return wrapper


@add_blink_tag
def foo(parameter1, parameter2):
    return f"{parameter1} ({parameter2})"


print(foo('ActiveCampaign!', 'Is awesome!'))

# Output: <blink>ActiveCampaign! (Is awesome!)</blink>
