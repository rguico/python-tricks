class Campaign:
    def __init__(self, message_id, user_id):
        self._message_id = message_id
        self._user_id = user_id

    def __repr__(self):
        """An unambiguous representation of the class."""
        return f"Campaign(message_id={self._message_id}, user_id={self._user_id})"

    def __str__(self):
        """A readable represenatation of the class."""
        return f"""
Campaign
    message_id: {self._message_id}
    user_id: {self._user_id}"""


c = Campaign(message_id=1, user_id=2)
print("__repr__ output: " + repr(c) + "\n")
print(f"string output: {c}")

# Output:
#
# __repr__ output: Campaign(message_id=1, user_id=2)
#
# string output:
# Campaign
#   message_id: 1
#   user_id: 2
