from abc import ABCMeta, abstractmethod


class MetricService(metaclass=ABCMeta):
    @abstractmethod
    def increment(self, name):
        pass


class DataDogService(MetricService):
    def histogram(self, name):
        print(f"HISTOGRAM metric called for: {name}!")


d = DataDogService()
d.gauge('foo')
